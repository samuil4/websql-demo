/**
 * Master application
 */


 /*
  * TODO: Router on hashchange, bind/unbind events: login,register; **extra: models
  */
window.Env = window.Env || {};

(function(GLOBAL, $){

    var App = {},
        routes;

    /*
     * Script urls
    */
    GLOBAL.Env.baseUrl = 'app/';
    GLOBAL.Env.jsUrl = GLOBAL.Env.baseUrl + 'js/';
    GLOBAL.Env.vendorUrl =  GLOBAL.Env.jsUrl + 'vendor/';
    GLOBAL.Env.viewsUrl = GLOBAL.Env.baseUrl + 'views/';
    GLOBAL.Env.modelsUrl = GLOBAL.Env.baseUrl + 'models/';
    GLOBAL.Env.templatesUrl = GLOBAL.Env.baseUrl + 'templates/';
    GLOBAL.Env.coreUrl = GLOBAL.Env.baseUrl + 'core/';
    GLOBAL.Env.version = '0.0.9';

    App.init = function initialiseAplication() {

        App.router = new Router(routes);

        $LAB.script(function() {
          App.router.loadResource({
                scriptPath: GLOBAL.Env.modelsUrl + 'user-model',
                type: 'Model',
                name: 'user',
                namespace: 'models'
            });
        }).wait(function(){

            //Initialize router when all resources are loaded and initialized
            $(document).trigger('route:init');

            App.router.loadResource({
                scriptPath: GLOBAL.Env.viewsUrl + 'header-view',
                type: 'View',
                name: 'header',
                namespace: 'views',
                models: ['user']
            });
        });
    };

    App.extendView = function(targetView) {
        targetView.prototype = Object.create(BaseView.prototype);
    };

    /*
     * Protoscript loading
    */
    $LAB
    .setOptions({AlwaysPreserveOrder:true})
    .script(
        [GLOBAL.Env.jsUrl + "plugins.js",
         GLOBAL.Env.jsUrl + "router.js",
         GLOBAL.Env.vendorUrl + "alertify.min.js",
         GLOBAL.Env.vendorUrl + "sdb_manager.js",
         GLOBAL.Env.vendorUrl + "underscore.js",
         GLOBAL.Env.coreUrl + "base-view.js",
         GLOBAL.Env.coreUrl + "base-model.js"
        ]
    )
    .wait(App.init);

    var routes = {
        'login': function loginRoute() {

            var router = this;

            console.log('login');

            if(router.views.login) {
                //to do
            } else {
                // scriptPath, type, name, namespace
                router.loadResource({
                    scriptPath: GLOBAL.Env.viewsUrl + 'login-view',
                    type: 'View',
                    name: 'login',
                    namespace: 'views',
                    models: ['user']
                });
            }
        },

        'register': function registerRoute() {

            console.log('register');

            var router = this;

            if( router.views.register ) {
                // do as you please
            } else {
                // scriptPath, type, name, namespace
                router.loadResource({
                    scriptPath: GLOBAL.Env.viewsUrl + 'register-view',
                    type: "View",
                    name: "register",
                    namespace: "views",
                    models: ['user']
                });
            }
        },

        'viewDocuments': function viewDocumentsRoute() {

            console.log('viewDocuments');
        },

        'default': function defaultRoute() {

            console.log('default');
        }

    };

    GLOBAL.App = App;

    /**
     * Document ready
     */
    $(function() {

    });

})(window, jQuery);