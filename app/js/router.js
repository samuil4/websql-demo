/*
 * Router class
*/

(function(GLOBAL, $LAB) {

    function Router(routes) {

        this.views = {};
        this.models = {};

        this.routes = routes;

        $(document).on('route:init', this.routerInit.bind(this));
        $(document).on('route:change', this.handleRouteChange.bind(this));
    }

    Router.prototype.routerInit = function initializingRouter() {

        console.info("%c init router","color:red");
        this.getRoute();
        this.changeRoute();
    };
    Router.prototype.handleRouteChange = function handlingRouteChange(evnt, params) {

        this.setRoute(params.newRoute);

    };
    Router.prototype.getRoute = function getCurrentRouting () {

        this.currentRoute = GLOBAL.location.hash.replace('#', '');
    };

    Router.prototype.changeRoute = function changeRouter() {

        // login, register, viewDocuments
        if(this.routes.hasOwnProperty(this.currentRoute)) {

            this.routes[this.currentRoute].call(this);
        } else {

            this.routes['default']();
        }
    };

    Router.prototype.setRoute = function setRouter(newRoute) {

        if(newRoute !== this.currentRoute) {

            //back reference to view on hashchange

            $(document).trigger("view:"+this.currentRoute+":exit");

            GLOBAL.location.hash = newRoute;
            this.currentRoute = newRoute;
            this.changeRoute();

            $(document).trigger("view:"+newRoute+":enter");
        } else {

            $(document).trigger("view:"+this.currentRoute+":update");
            // console.error('ne klikai kat malumnik FE');
        }

    };

    Router.prototype.loadResource = function loadingResource(params) {
        // scriptPath, type, name, namespace, model
        // model - expected to be loaded and instantiated in the router
        var router = this;

        $LAB.script(params.scriptPath + '.js').wait(function(){

            var resourceConfig = {};
            if(params.models){

                resourceConfig.model = {};
                params.models.forEach(function(modelName){

                    if(router.models[modelName]) {
                        resourceConfig.model[modelName] = router.models[modelName];
                    }
                });

            }
            if(params.type === "model") {

                resourceConfig.modelName = params.name;
            }
            router[params.namespace] = router[params.namespace] || {};
            router[params.namespace][params.name] = new GLOBAL[params.namespace][params.name + params.type](resourceConfig);
        });
    };




    GLOBAL.Router = Router;

})(window, $LAB);