//user model

(function(GLOBAL) {

    function userModel(options) {

        console.log("user-model");

        this.name = localStorage.userName;
        this.id = localStorage.userId;
        this.password;
        this.loginState = this.id && this.name ? true : false;

        this.modelName = options.modelName;

    }

    userModel.prototype.setName = function settingName(name){

        this.name = name;
        this.changed();
    };

    userModel.prototype.setPassword = function settingPassword(password){

        this.password = password;
        this.changed();
    };

    userModel.prototype.changed = function modelHasChanged() {

        $(document).trigger("model:" + this.modelName + ":update");
    };

    GLOBAL.models = GLOBAL.models || {};
    GLOBAL.models.userModel = userModel;

})(window);