/***
 * Base view class
 */

window.Env = window.Env || {};

(function(GLOBAL, $){

    function BaseView(options) {

        var view = this;
        view.name = options.name;

        if(options.model) {

            //if model is present - assign to view
            view.model = options.model;
        }

        view.template = localStorage['template-' + view.name + '?v=' + GLOBAL.Env.version] || '';

        if(view.template){
            view.template = _.template(view.template);
        }

        console.info('init ' + view.name +  ' view');
        view.placeholder = document.getElementById('placeholder-'+view.name);
        view.$placeholder = $(view.placeholder);
        view.$placeholder.on('view:' + view.name + ':render', view.render.bind(view));

        $(document).on('view:' + view.name + ':enter', this.enterView.bind(this));
        $(document).on('view:' + view.name + ':exit', this.exitView.bind(this));
        $(document).on('view:' + view.name + ':update', this.updateView.bind(this));

        if( !view.template ) {
            view.loadTemplate();
        } else {
            console.info('using cached ' + view.name + ' template');
            view.render();
        }

        console.info('init' + view.name +  'view finished!');
    }

    BaseView.prototype.extend = function extendingBaseView(view) {

        // view.prototype = Object.create(BaseView.prototype);
    };
    /* Render function injects the template to the DOM element
     * returns void
     */
    BaseView.prototype.render = function rendering() {

        var view = this;

        view.unbindEvents();

        console.info('Rendering ' + view.name + ' view!');
        view.placeholder.innerHTML = view.template.call(view);

        //to do: fix this!

        view.bindEvents();
    };

    BaseView.prototype.loadTemplate = function templateLoading () {
        var view = this,
            xhr = new XMLHttpRequest();

        xhr.open('GET', GLOBAL.Env.templatesUrl + view.name +'.html', true);
        xhr.onreadystatechange= function() {
            if (this.readyState!==4 || this.status!==200) return;
            console.info('using loaded ' + view.name + ' template');
            view.template = localStorage['template-' + view.name + '?v=' + GLOBAL.Env.version] = this.responseText;
            view.template = _.template(view.template);
            view.$placeholder.trigger('view:' + view.name + ':render');
        };
        xhr.send();
    };


    BaseView.prototype.bindEvents = function bindingEvents () {

        console.log('%cbinding base view header events', 'color: blue;');
    };

    BaseView.prototype.unbindEvents = function unbindingEvents () {

        console.log('%cunbinding base view header events', 'color: blue;');
    };

    BaseView.prototype.enterView = function enteringView() {

        console.info("entering " + this.name + " view");
    };

    BaseView.prototype.exitView = function exitingView() {

        console.info("exiting " + this.name + " view");
    };

    BaseView.prototype.updateView = function updatingView() {

        console.info("updating " + this.name + " view");
    };

    GLOBAL.BaseView = BaseView;

})(window, jQuery);